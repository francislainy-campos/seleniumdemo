package selenium_demo.hooks;

import selenium_demo.test_rail.APIClient;
import selenium_demo.test_rail.APIException;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.After;
import selenium_demo.DriverFactory;

import cucumber.runtime.ScenarioImpl;
import gherkin.formatter.model.Result;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static selenium_demo.test_rail.TestRailAccount.testRailApiClient;
import static selenium_demo.utils.Util.getProperties;

public class Hooks {

    private static APIClient client = null;
    private static final String runId = getProperties().getProperty("runIdTestRail");
    private static final int FAIL_STATE = 5;
    private static final int SUCCESS_STATE = 1;
    private static final String SUCCESS_COMMENT = "This test passed with Selenium";
    private static final String FAILED_COMMENT = "This test failed with Selenium";

    @Rule
    public TestName testName = new TestName();

    public static WebDriver driver;

    @Before
    public void initializeTest() {
        client = testRailApiClient();
        driver = DriverFactory.startDriver();
    }

    @After()
    public void tearDown(Scenario scenario) {
        String caseId = "";

        for (String s : scenario.getSourceTagNames()) {
            if (s.contains("TestRail")) {

                String[] res = s.split("(\\(.*?)");

                caseId = res[1].substring(0, res[1].length() - 1); // Removing the last parenthesis
            }
        }

        Map<String, java.io.Serializable> data = new HashMap<>();

        if (!scenario.isFailed()) {
            data.put("status_id", SUCCESS_STATE);
            data.put("comment", SUCCESS_COMMENT);

        } else {
            data.put("status_id", FAIL_STATE);
            data.put("comment", logError(scenario));
        }

        if (!caseId.equals("")) {
            try {
                client.sendPost("add_result_for_case/" + runId + "/" + caseId, data);
            } catch (IOException | APIException e) {
                e.printStackTrace();
            }
        }

        driver.close();
        driver.quit();
    }

    private static String logError(Scenario scenario) {
        Field field = FieldUtils.getField(((ScenarioImpl) scenario).getClass(), "stepResults", true);
        field.setAccessible(true);
        try {
            ArrayList<Result> results = (ArrayList<Result>) field.get(scenario);
            for (Result result : results) {
                if (result.getErrorMessage() != null)
                    if (result.getErrorMessage().length() >= 350) {
                        return FAILED_COMMENT + "\n" + result.getErrorMessage().substring(0, 350);
                    } else {
                        return FAILED_COMMENT + "\n" + result.getErrorMessage();
                    }
            }
        } catch (Exception e) {
            return FAILED_COMMENT;
        }

        return FAILED_COMMENT;
    }


}