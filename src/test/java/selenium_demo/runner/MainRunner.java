package selenium_demo.runner;

import selenium_demo.managers.FileReaderManager;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.File;
@RunWith(Cucumber.class)
@CucumberOptions(
//        features = {"src/test/java/selenium_demo/feature_files/"},
        features = {"."}, // Leaving the "." as the path to be able to populate the rerun.txt file with the whole path for the feature files that fail https://stackoverflow.com/a/40326021/12439311
        glue = {"selenium_demo.steps", "selenium_demo.hooks"},
        monochrome = true,
        tags = {"@regression"},
        plugin = {"pretty", "rerun:target/rerun.txt", "html:target/cucumber", "json:target/cucumber.json", "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"}
)

public class MainRunner {

	@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(new File(FileReaderManager.getInstance().getConfigReader().getReportConfigPath()));
	}

}
