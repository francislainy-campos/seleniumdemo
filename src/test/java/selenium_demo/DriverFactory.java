package selenium_demo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import selenium_demo.utils.OSValidator;

public class DriverFactory {

    private static WebDriver driver;

    public static WebDriver startDriver() {

        String projectLocation = System.getProperty("user.dir");

        // add in elements for logging into the mobile application also - Android and
        // iOS.
        if (OSValidator.isMac()) {
            System.setProperty("webdriver.chrome.driver", projectLocation + "/chromedriver_mac");
        } else if (OSValidator.isWindows()) {
            System.setProperty("webdriver.chrome.driver", projectLocation + "/chromedriver.exe");
        } else {
            System.setProperty("webdriver.chrome.driver", projectLocation + "/chromedriver_linux");
        }

        driver = new ChromeDriver();
        driver.manage().window().maximize();

        return driver;
    }

}
