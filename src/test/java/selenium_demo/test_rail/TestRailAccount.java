package selenium_demo.test_rail;

import java.util.Properties;

import static selenium_demo.utils.Util.getPassProperties;
import static selenium_demo.utils.Util.getProperties;

public class TestRailAccount {

    public static APIClient testRailApiClient() {

        Properties prop = getProperties();
        Properties passProp = getPassProperties();


        String baseUrl = prop.getProperty("baseUrl");
        String usernameTestRail = passProp.getProperty("usernameTestRail");
        String passwordTestRail = passProp.getProperty("passwordTestRail");

        if (System.getenv("usernameTestRail") != null && System.getenv("passwordTestRail") != null) { // If coming from Jenkins..
            System.out.println(System.getenv("usernameTestRail"));
            usernameTestRail = System.getenv("usernameTestRail");
            passwordTestRail = System.getenv("passwordTestRail");
        } else {
            System.out.println("JENKINS VAR NULL!");
        }

        APIClient client = new APIClient(baseUrl);
        client.setUser(usernameTestRail);
        client.setPassword(passwordTestRail);

        return client;
    }

}
