package selenium_demo.test_rail;

public class APIException extends Exception
{
    public APIException(String message)
    {
        super(message);
    }
}
