package selenium_demo.steps.login;


import cucumber.api.java.en.Given;
import selenium_demo.pages.login.LoginPage;

import static selenium_demo.pages.InitPages.startBrowserAndLaunch;

public class LoginSteps {


    @Given("^I launched the landing page for the application$")
    public static void iLaunchedTheLandingPageForTheApplication() {
        startBrowserAndLaunch();
    }

    @Given("^I click the sign in button on the top banner$")
    public void iClickTheSignInButtonOnTheTopBanner() {
        LoginPage.clickSignInButton();
    }
}