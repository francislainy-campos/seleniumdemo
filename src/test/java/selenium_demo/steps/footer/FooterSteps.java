package selenium_demo.steps.footer;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import selenium_demo.pages.base_page.BasePage;
import selenium_demo.pages.footer.FooterPage;

import static junit.framework.TestCase.assertTrue;

public class FooterSteps {

    @When("^I add \"([^\"]*)\" to the news letter section$")
    public void iAddToTheNewsLetterSection(String word) {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^an \"([^\"]*)\" is shown under the search box$")
    public void anIsShownUnderTheSearchBox(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I am navigated to the \"([^\"]*)\" page$")
    public void iAmNavigatedToThePage(String page) {
        assertTrue(BasePage.isOnPage(page));
    }

    @Then("^I can see the footer section with the following items$")
    public void iCanSeeTheFooterSectionWithTheFollowingItems(DataTable dt) {
        assertTrue(FooterPage.allItemsDisplayed(dt));
    }

    @Then("^the following icons are displayed$")
    public void theFollowingIconsAreDisplayed(DataTable dt) {
        assertTrue(FooterPage.areSocialMediaIconsDisplayed(dt));
    }

    @And("^I click \"([^\"]*)\" under the footer section$")
    public void iClickUnderTheFooterSection(String item) {
        FooterPage.clickItem(item);
    }

    @Then("^I am navigated to the correct page for that selected \"([^\"]*)\"$")
    public void iAmNavigatedToTheCorrectPageForThatSelected(String item) {
      assertTrue(FooterPage.isUserOnCorrectUrl(item));
    }
}
