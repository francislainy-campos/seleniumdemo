Feature: Footer links on the Automation Practice Application

  Description: As a user I should be able to see and interact with links displayed on the footer of the page within the Automation Practice application

  Background:
    Given I launched the landing page for the application

  @footer @regression @regression @TestRail(2)
  Scenario: Footer container displays all links
    Then I can see the footer section with the following items
      | Women                       |
      | Specials                    |
      | New products                |
      | Best sellers                |
      | Our stores                  |
      | Contact us                  |
      | Terms and conditions of use |
      | About us                    |
      | Sitemap                     |
      | My orders                   |
      | My credit slips             |
      | My addresses                |
      | My personal info            |

  @footer @regression @TestRail(3)
  Scenario Outline: Footer items navigate the user to expected pages
    And I click "<item>" under the footer section
    Then I am navigated to the correct page for that selected "<item>"
    Examples:
      | item                        |
      | Women                       |
      | Specials                    |
      | New products                |
      | Best sellers                |
      | Our stores                  |
      | Contact us                  |
      | Terms and conditions of use |
      | About us                    |
      | Sitemap                     |
      | My orders                   |
      | My credit slips             |
      | My addresses                |
      | My personal info            |

  @footer @regression @TestRail(4)
  Scenario: Footer displays social media items
    Then the following icons are displayed
      | facebook    |
      | twitter     |
      | youtube     |
      | google plus |

  @footer @regression @TestRail(5)
  Scenario Outline: Footer displays accurate store information
    Then the footer displays "<item>" and "<text>"
    Examples:
      | item     | text                                                            |
      | location | Selenium Framework, Research Triangle Park, North Carolina, USA |
      | phone    | Call us now: (347) 466-7432                                     |
      | email    | Email: support@seleniumframework.com                            |

  @footer @regression @TestRail(6)
  Scenario Outline: Footer sections display under their correct header
    Then I can see "<item>" displayed under the "<header>" section
    Examples:
      | item                        | header      |
      | Women                       | Categories  |
      | Specials                    | Information |
      | New products                | Information |
      | Best sellers                | Information |
      | Our stores                  | Information |
      | Contact us                  | Information |
      | Terms and conditions of use | Information |
      | About us                    | Information |
      | Sitemap                     | Information |
      | My orders                   | My Account  |
      | My credit slips             | My Account  |
      | My addresses                | My Account  |
      | My personal info            | My Account  |

  @footer @regression @TestRail(7)
  Scenario Outline: User blocked from subscribing to news letter if invalid email
    When I add "<email>" to the news letter section
    Then I am navigated to the "landing" page
    And an "<error message>" is shown under the search box
    Examples:
      | email          | error message         |
      | email.com      | Invalid email address |
      | email@gmail    | Invalid email address |
      | email@gmailcom | Invalid email address |
