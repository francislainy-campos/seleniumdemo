Feature: Login to Application

  Description: As a user I should be able to log into the application

  Background:
    Given I launched the landing page for the application

  @login @regression @TestRail(1)
  Scenario: User able to navigate to login page
    Given I click the sign in button on the top banner
    Then I am navigated to the "login" page



