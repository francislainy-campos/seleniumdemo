package selenium_demo.config;

import java.util.Properties;

import static selenium_demo.utils.Util.getProperties;

public class ConfigFileReader {

    public String getReportConfigPath(){

        Properties prop = getProperties();
        String reportConfigPath = prop.getProperty("reportConfigPath");

        if(reportConfigPath!= null) return reportConfigPath;
        else throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");
    }
}
