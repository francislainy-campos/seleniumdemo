package selenium_demo.pages;

import selenium_demo.pages.base_page.BasePage;
import selenium_demo.pages.footer.FooterPage;
import selenium_demo.pages.landing.LandingPage;
import selenium_demo.pages.login.LoginPage;

import static org.openqa.selenium.support.PageFactory.initElements;
import static selenium_demo.hooks.Hooks.driver;

import java.util.Properties;

import static selenium_demo.utils.Util.getProperties;

public class InitPages {

    public static String currentUrl = "";

    public static void startBrowserAndLaunch() {

        selectAndLaunchUrl();
        initAllPages();
    }

    private static void selectAndLaunchUrl() {
        String urlFromProps = System.getProperty("app.url");

        Properties prop = getProperties();
        String env = prop.getProperty("env");

        System.out.println("THIS IS THE URL PASSED IN SYSTEM.GETPROPERTY('app.url'): " + urlFromProps);
        System.out.println("THIS IS THE ENV FROM PROPERTIES FILE: " + env);

        if (System.getProperty("app.env") != null) { // If coming from Jenkins we use System.getProperty..
            env = System.getProperty("app.env");
            System.out.println("THIS IS THE ENV FROM SYSTEM.GETPROPERTY('app.env'): " + System.getProperty("app.env"));
        }

        if (urlFromProps != null && !urlFromProps.equals("")) {
            currentUrl = urlFromProps;
        } else {
            if (env.equalsIgnoreCase("QA")) {
                currentUrl = getProperties().getProperty("qaUrl");
            } else if (env.equalsIgnoreCase("dev")) {
                currentUrl = getProperties().getProperty("devUrl");
            }
        }

        driver.get(currentUrl);
    }

    private static void initAllPages() {
        initElements(driver, InitPages.class);
        initElements(driver, LoginPage.class);
        initElements(driver, BasePage.class);
        initElements(driver, FooterPage.class);
        initElements(driver, LandingPage.class);
    }

}
