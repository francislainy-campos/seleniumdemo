package selenium_demo.pages.footer;

import cucumber.api.DataTable;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import selenium_demo.hooks.Hooks;

import java.util.*;
import java.util.stream.Collectors;

import static selenium_demo.utils.Util.clickWithActions;
import static selenium_demo.utils.Util.isElementDisplayed;

public class FooterPage {

    /**
     * Footer Links
     */
    @FindBy(linkText = "Women")
    public static WebElement womenLink;
    @FindBy(linkText = "Specials")
    public static WebElement specialsLink;
    @FindBy(linkText = "New products")
    public static WebElement newProductsLink;
    @FindBy(linkText = "Best sellers")
    public static WebElement bestSellersLink;
    @FindBy(linkText = "Our stores")
    public static WebElement outStoresLink;
    @FindBy(linkText = "Contact us")
    public static WebElement contactUsLink;
    @FindBy(linkText = "Terms and conditions of use")
    public static WebElement termsLink;
    @FindBy(linkText = "About us")
    public static WebElement aboutUsLink;
    @FindBy(linkText = "Sitemap")
    public static WebElement sitemapLink;
    @FindBy(linkText = "My orders")
    public static WebElement myOrdersLink;
    @FindBy(linkText = "My credit slips")
    public static WebElement myCreditLink;
    @FindBy(linkText = "My addresses")
    public static WebElement myAddressesLink;
    @FindBy(linkText = "My personal info")
    public static WebElement myPersonalInfoLink;


    /**
     * Social media icons
     */
    @FindBy(className = "facebook")
    public static WebElement facebookIcon;
    @FindBy(className = "twitter")
    public static WebElement twitterIcon;
    @FindBy(className = "youtube")
    public static WebElement youtubeIcon;
    @FindBy(className = "google-plus")
    public static WebElement googlePlusIcon;


    public static boolean allItemsDisplayed(DataTable dt) {


        List<String> list = dt.cells(0).stream().flatMap(Collection::stream).collect(Collectors.toList());
        WebElement[] elements = new WebElement[]{womenLink, specialsLink, newProductsLink, bestSellersLink, outStoresLink, contactUsLink, termsLink, aboutUsLink, sitemapLink, myOrdersLink, myCreditLink, myAddressesLink, myPersonalInfoLink};


        HashMap<String, WebElement> map = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            map.put(list.get(i), elements[i]);
        }

        for (Map.Entry<String, WebElement> entry : map.entrySet()) {

            if (!isElementDisplayed(entry.getValue())) {
                return false;
            }

        }

        return true;

    }

    public static void clickItem(String itemTitle) {

        clickWithActions(itemBasedOnText(itemTitle));
    }

    public static boolean isUserOnCorrectUrl(String itemName) {

        String urlPiece = "";
        switch (itemName.toLowerCase()) {
            case "women":
                urlPiece = "category";
                break;
            case "specials":
                urlPiece = "prices-drop";
                break;
            case "new products":
                urlPiece = "new-products";
                break;
            case "best sellers":
                urlPiece = "best-sales";
                break;
            case "our stores":
                urlPiece = "stores";
                break;
            case "contact us":
                urlPiece = "contact";
                break;
            case "terms and conditions of use":
                urlPiece = "cms";
                break;
            case "about us":
                urlPiece = "cms";
                break;
            case "sitemap":
                urlPiece = "sitemap";
                break;
            case "my orders":
                urlPiece = "history";
                break;
            case "my credit slips":
                urlPiece = "order-slip";
                break;
            case "my addresses":
                urlPiece = "addresses";
                break;
            case "my personal info":
                urlPiece = "identity";
                break;
            default:
                urlPiece = null;
        }

        return Hooks.driver.getCurrentUrl().contains(urlPiece);

    }


    public static boolean areSocialMediaIconsDisplayed(DataTable dt) {

        List<String> list = dt.cells(0).stream().flatMap(Collection::stream).collect(Collectors.toList());
        WebElement[] elements = new WebElement[]{facebookIcon, twitterIcon, youtubeIcon, googlePlusIcon};


        HashMap<String, WebElement> map = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            map.put(list.get(i), elements[i]);
        }

        for (Map.Entry<String, WebElement> entry : map.entrySet()) {

            if (!isElementDisplayed(entry.getValue())) {
                return false;
            }

        }

        return true;


    }

    private static WebElement itemBasedOnText(String text) {
        switch (text.toLowerCase()) {
            case "women":
                return womenLink;
            case "specials":
                return specialsLink;
            case "new products":
                return newProductsLink;
            case "best sellers":
                return bestSellersLink;
            case "our stores":
                return outStoresLink;
            case "contact Us":
                return contactUsLink;
            case "terms and conditions of use":
                return termsLink;
            case "about us":
                return aboutUsLink;
            case "sitemap":
                return sitemapLink;
            case "my orders":
                return myOrdersLink;
            case "my credit slips":
                return myCreditLink;
            case "my addresses":
                return myAddressesLink;
            case "my personal info":
                return myPersonalInfoLink;
            default:
                return null;
        }
    }

}
