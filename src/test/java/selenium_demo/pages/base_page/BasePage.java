package selenium_demo.pages.base_page;

import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import selenium_demo.pages.InitPages;

import static selenium_demo.hooks.Hooks.driver;

public class BasePage {

    @FindBy(xpath = "//cdk-virtual-scroll-viewport")
    public static WebElement myElement;

    public static boolean myElementIsDisplayed() {

        try {
            return myElement.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }

    }


    public static boolean isOnPage(String page) {

        String url = InitPages.currentUrl;
        switch (page) {
            case "landing":
//                url = url;
                break;
            case "login":
                url = url + "?controller=authentication&back=my-account";
                break;

        }

        return driver.getCurrentUrl().equals(url);
    }
}














