package selenium_demo.pages.login;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static selenium_demo.utils.Util.clickWithActions;

public class LoginPage {

    @FindBy(linkText = "Sign in")
    public static WebElement signInButton;

    public static void clickSignInButton() {
        clickWithActions(signInButton);
    }
}
