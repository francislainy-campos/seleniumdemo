package selenium_demo.utils;

import org.openqa.selenium.*;
import selenium_demo.DriverFactory;
import selenium_demo.hooks.Hooks;
import selenium_demo.test_rail.TestRailAccount;
import org.joda.time.DateTime;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class Util {

    public static WebDriver init(WebDriver wd) {
        wd = DriverFactory.startDriver();
        return wd;
    }

    public static Properties getProperties() {
        return loadProp("config.properties");
    }

    public static Properties getPassProperties() {
        return loadProp("pass.properties");
    }

    private static Properties loadProp(String propFileName) {
        Properties prop = new Properties();

        InputStream inputStream = TestRailAccount.class.getClassLoader().getResourceAsStream(propFileName);

        if (inputStream != null) {
            try {
                prop.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        return prop;
    }

    public static boolean isElementDisplayed(WebElement e) {
        try {
            return e.isDisplayed();
        } catch (NoSuchElementException ex) {
            return false;
        }
    }

    public static void waitImplicit(WebDriver wd, int time) {
        wd.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
    }

    public static WebElement waitExplicitely(WebDriver wd, By by) {

        WebDriverWait wait = new WebDriverWait(wd,10);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static void checkElementIsDisplayed(WebElement element) {
        assertTrue(element.isDisplayed());
    }

    public static void checkElementIsNotDisplayed(List<WebElement> elementList) {
        assertFalse(elementList.size() > 0); // If true element not displayed
    }

    public static JavascriptExecutor clickWithJS(WebDriver wd, WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) wd;
        js.executeScript("arguments[0].click();", element);
        return js;
    }

    public static void clickWithActions(WebElement element) {
        Actions actions = new Actions(Hooks.driver);
        actions.moveToElement(element).click().perform();
    }

    public static void scrollInto(WebDriver wd, WebElement element) {

        ((JavascriptExecutor) wd).executeScript("arguments[0].scrollIntoView();", element);
    }

    public static String currentDateAsString() {

        ZoneId zoneId = ZoneId.of("Europe/London");  // Or ZoneOffset.UTC or ZoneId.systemDefault()
        String today = LocalDateTime.now(zoneId).toString();

        return new DateTime(today).toString("dd-MMM-yyyy HH:mm");
    }

    public static void clearElement(WebElement element) {
        element.clear();
    }

    public static void sleepForTime(int timeInMilliseconds) {
        try {
            Thread.sleep(timeInMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void clickCatchingStaleException(WebElement element) {

        try {
            clickWithActions(element);
        } catch (Exception e) {
            clickWithActions(element);
        }
    }
}

