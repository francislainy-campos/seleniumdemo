## Selenium Demo (Selenium, Maven, Cucumber, JUnit, Extent Reports and TestRail integration using Page Object Model design pattern).

This project contains the source code for Selenium Demo, which aims to demonstrate how to use automation tools such as Selenium, Cucumber BDD, JUnit, Maven, Extent Reports, and TestRail integration on the context of testing a web application.

### Project Walkthrough

* The project contains dependencies and libraries managed through the POM.xml file as any normal Maven project. The code is structured using Page Object Model and Page Object Factory,
as well as feature files, steps and pages classes for each section or page to be tested stored under their respective folder directories. 

* As per the Page Object structure, the interaction with the elements should also be handled within the page files. Assertions should be handled within the Steps classes.

* Common pieces of code shared between classes are stored under a `Util class` file. 

* *Each page* should be initiated within the `InitPages` class, to avoid **Null Pointer Exception**.

* Password details for TestRail and others need to be configured within the `pass.properties` file

* Environments and other configurations can be managed through `config.properties` file or retrieved when sent through MVN command line (e.g: `mvn test -Dapp.env=dev`)

* The connection with TestRail (test management tool) is managed within the test_rail package and `Hooks` file. 
Need to use valid test case ids under the @TestRail tag so the connection is successful as well as proper account details.




